from pprint import pprint
from collections import defaultdict


# CC 11
def find_duplicates1(data):
    result = {'data': [], 'duplicates_full': [], 'duplicates_id': [], 'duplicates_fio': []}
    verified_data = []
    seen_id = {}
    seen_fio = {}
    for item in data:
        if item in verified_data:
            result['duplicates_full'].append(item)
        else:
            verified_data.append(item)
            if item['id'] in seen_id:
                result['duplicates_id'].append(item)
                if seen_id[item['id']] not in result['duplicates_id']:
                    result['duplicates_id'].append(seen_id[item['id']])
            else:
                seen_id[item['id']] = item
            if item['fio'] in seen_fio:
                result['duplicates_fio'].append(item)
                if seen_fio[item['fio']] not in result['duplicates_fio']:
                    result['duplicates_fio'].append(seen_fio[item['fio']])
            else:
                seen_fio[item['fio']] = item
    duplicates_list = [item for sublist in result.values() for item in sublist]
    result['data'] = [item for item in data if item not in duplicates_list]
    return result


# CC 27
def find_duplicates2(data):
    def flat_list(list_of_lists):
        return [item for sublist in list_of_lists for item in sublist]

    groups = {}
    for key in ['duplicates_full', 'duplicates_fio', 'duplicates_id']:
        groups[key] = defaultdict(list)
    for i, item in enumerate(data):
        id, fio = item['id'], item['fio']
        [groups['duplicates_full'][key].append(i) for key in
         set([k for k in groups['duplicates_full'].keys() if f'{id}_{fio}' in k] + [f'{id}_{fio}'])]
        [groups['duplicates_fio'][key].append(i) for key in
         set([k for k in groups['duplicates_fio'].keys() if fio in k] + [fio])]
        groups['duplicates_id'][id].append(i)
    fulls = flat_list([v for k, v in groups['duplicates_full'].items() if len(v) > 1])
    result = {'duplicates_full': [v[0] for k, v in groups['duplicates_full'].items() if len(v) > 1],
              'duplicates_fio': flat_list([v for k, v in groups['duplicates_fio'].items() if len(v) > 1]),
              'duplicates_id': flat_list([v for k, v in groups['duplicates_id'].items() if len(v) > 1])}
    result['data'] = [v for k, v in enumerate(data) if k not in set(flat_list(result.values()))]
    result['duplicates_id'] = [i for i in set(result['duplicates_id']) if
                               not (i in fulls and i not in result['duplicates_full'])]
    result['duplicates_fio'] = [i for i in set(result['duplicates_fio']) if
                                not (i in fulls and i not in result['duplicates_full'])]
    for key in ['duplicates_fio', 'duplicates_id', 'duplicates_full']:
        result[key] = [data[v] for v in result[key]]
    return result


if __name__ == '__main__':
    a = [
        {'id': 1, 'fio': 'Иванченко Александр Александрович'},
        {'id': 2, 'fio': 'Быков Михаил Иванович'},
        {'id': 3, 'fio': 'Басова Василиса Леонидовна'},
        {'id': 3, 'fio': 'Басова Василиса Леонидовна'},
        {'id': 2, 'fio': 'Иванченко Александр Александрович'},
        {'id': 3, 'fio': 'Иванченко Александр Александрович'},
        {'id': 100, 'fio': 'Новый'}
    ]

    pprint(find_duplicates1(a))
    print()
    pprint(find_duplicates2(a))
