from pprint import pprint


# Cyclomatic complexity = 5
def duplicate_finder(data) -> dict:

    def find_full_duplicates(the_data) -> list:
        temp = []
        full_duplicates = []
        for item in the_data:
            if item in temp:
                full_duplicates.append(item)
            temp.append(item)
        return full_duplicates

    def find_by_key(the_data, parameter: str) -> list:
        seen = {}
        duplicates = []

        for item in the_data:
            value = item[parameter]
            if value in seen:
                duplicates.append(item)
                if seen[value] not in duplicates:
                    duplicates.append(seen[value])
            else:
                seen[value] = item

        return duplicates

    result = {'data': [],
              'duplicates_full': find_full_duplicates(data),
              'duplicates_fio': find_by_key(data, 'fio'),
              'duplicates_id': find_by_key(data, 'id')}

    duplicates_list = [item for sublist in result.values() for item in sublist]
    result['data'] = [item for item in data if item not in duplicates_list]

    return result


if __name__ == '__main__':
    a = [
        {'id': 1, 'fio': 'Иванченко Александр Александрович'},
        {'id': 2, 'fio': 'Быков Михаил Иванович'},
        {'id': 3, 'fio': 'Басова Василиса Леонидовна'},
        {'id': 3, 'fio': 'Басова Василиса Леонидовна'},
        {'id': 2, 'fio': 'Иванченко Александр Александрович'},
        {'id': 3, 'fio': 'Иванченко Александр Александрович'},
        {'id': 100, 'fio': 'Новый'}
    ]

    pprint(duplicate_finder(a))
