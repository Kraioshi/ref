from source.source import find_duplicates1


def test_no_duplicates():
    data = [
        {'id': 1, 'fio': 'Name Nameovich'},
        {'id': 2, 'fio': 'Surname Surnameovich'}
    ]
    result = find_duplicates1(data)
    assert result['data'] == data
    assert len(result['data']) == 2
    assert result['duplicates_full'] == []
    assert result['duplicates_fio'] == []
    assert result['duplicates_id'] == []


def test_duplicates_id():
    data = [
        {'id': 1, 'fio': 'John Doe'},
        {'id': 2, 'fio': 'Jane Doe'},
        {'id': 1, 'fio': 'Jonathan DDos'}
    ]
    result = find_duplicates1(data)
    assert result['data'] == [{'id': 2, 'fio': 'Jane Doe'}]
    assert result['duplicates_full'] == []
    assert result['duplicates_fio'] == []
    assert result['duplicates_id'] == [{'fio': 'Jonathan DDos', 'id': 1}, {'fio': 'John Doe', 'id': 1}]
    assert len(result['duplicates_id']) == 2


def test_duplicates_fio():
    data = [
        {'id': 1, 'fio': 'John Doe'},
        {'id': 2, 'fio': 'Jane Doerkova'},
        {'id': 3, 'fio': 'John Doe'}
    ]
    result = find_duplicates1(data)
    assert result['data'] == [{'id': 2, 'fio': 'Jane Doerkova'}]
    assert result['duplicates_full'] == []
    assert len(result['duplicates_fio']) == 2
    assert result['duplicates_fio'] == [{'fio': 'John Doe', 'id': 3}, {'fio': 'John Doe', 'id': 1}]
    assert result['duplicates_id'] == []


def test_duplicates_full():
    data = [
        {'id': 1, 'fio': 'Abby Babby'},
        {'id': 1, 'fio': 'Abby Babby'}
    ]
    result = find_duplicates1(data)
    assert result['data'] == []
    assert len(result['duplicates_full']) == 1
    assert result['duplicates_full'] ==  [{'id': 1, 'fio': 'Abby Babby'}]
    assert result['duplicates_fio'] == []
    assert result['duplicates_id'] == []

