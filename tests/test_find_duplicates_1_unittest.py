import unittest
from source.source import find_duplicates1


class TestFindDuplicates1(unittest.TestCase):

    def test_no_duplicates(self):
        data = [
            {'id': 1, 'fio': 'Name Nameovich'},
            {'id': 2, 'fio': 'Surname Surnameovich'}
        ]
        result = find_duplicates1(data)
        self.assertEqual(result['data'], data)
        self.assertEqual(len(result['data']), 2)
        self.assertEqual(result['duplicates_full'], [])
        self.assertEqual(result['duplicates_fio'], [])
        self.assertEqual(result['duplicates_id'], [])

    def test_duplicates_id(self):
        data = [
            {'id': 1, 'fio': 'John Doe'},
            {'id': 2, 'fio': 'Jane Doe'},
            {'id': 1, 'fio': 'Jonathan DDos'}
        ]
        result = find_duplicates1(data)
        self.assertEqual(result['data'], [{'id': 2, 'fio': 'Jane Doe'}])
        self.assertEqual(result['duplicates_full'], [])
        self.assertEqual(result['duplicates_fio'], [])
        self.assertEqual(result['duplicates_id'], [{'fio': 'Jonathan DDos', 'id': 1}, {'fio': 'John Doe', 'id': 1}])
        self.assertEqual(len(result['duplicates_id']), 2)

    def test_duplicates_fio(self):
        data = [
            {'id': 1, 'fio': 'John Doe'},
            {'id': 2, 'fio': 'Jane Doerkova'},
            {'id': 3, 'fio': 'John Doe'}
        ]
        result = find_duplicates1(data)
        self.assertEqual(result['data'], [{'id': 2, 'fio': 'Jane Doerkova'}])
        self.assertEqual(result['duplicates_full'], [])
        self.assertEqual(len(result['duplicates_fio']), 2)
        self.assertEqual(result['duplicates_fio'], [{'fio': 'John Doe', 'id': 3}, {'fio': 'John Doe', 'id': 1}])
        self.assertEqual(result['duplicates_id'], [])

    def test_duplicates_full(self):
        data = [
            {'id': 1, 'fio': 'Abby Babby'},
            {'id': 1, 'fio': 'Abby Babby'}
        ]
        result = find_duplicates1(data)
        self.assertEqual(result['data'], [])
        self.assertEqual(len(result['duplicates_full']), 1)
        self.assertEqual(result['duplicates_full'], [{'id': 1, 'fio': 'Abby Babby'}])
        self.assertEqual(result['duplicates_fio'], [])
        self.assertEqual(result['duplicates_id'], [])


if __name__ == '__main__':
    unittest.main()
